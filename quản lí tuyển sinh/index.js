function ketQua() {
    // 3 môn
    var diemToan = document.getElementById("diem-toan").value * 1;
    var diemVan = document.getElementById("diem-van").value * 1;
    var diemAnh = document.getElementById("diem-anh").value * 1;
    // đối tượng,khu vực
    var doiTuong = document.getElementById("doi-tuong").value;
    var khuVuc = document.getElementById("khu-vuc").value;
    // điểm chuẩn
    var diemChuan = document.getElementById("diem-chuan").value * 1;
    // điểm tổng
    var diemTong = diemVan + diemAnh + diemToan;
    document.getElementById("diem-chuan ", diemChuan)
    // điểm ưu tiên theo khu vực
    switch (khuVuc) {
        case "a":
            diemTong += 2;
            break;
        case "b":
            diemTong += 1;
            break;
        case "c":
            diemTong += 0.5;
            break;
    }
    // điểm ưu tiên theo đối tượng
    switch (doiTuong) {
        case "1":
            diemTong += 2.5;
            break;
        case "2":
            diemTong += 1.5;
            break;
        case "3":
            diemTong += 1;
            break;
    }
    // so sánh điểm tổng với điểm chuẩn
    if (diemTong >= diemChuan) {
        document.getElementById("result").innerHTML = ` Tổng Điểm: ${diemTong} <br/> Bạn Đã Trúng Tuyển`;
    } else {
        document.getElementById("result").innerHTML = `Tổng Điểm: ${diemTong}  <br/> Bạn Đã Rớt Tuyển`;
    }
}